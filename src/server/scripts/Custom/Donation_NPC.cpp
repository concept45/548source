#include "ScriptPCH.h"


const uint32 APEXIS_SHARD = 32569;

class Donation_NPC : public CreatureScript
{
public:

	Donation_NPC() : CreatureScript("Donation_NPC") {}

	uint32 SelectGold(Player* player)
	{
		QueryResult select = LoginDatabase.PQuery("SELECT gold cms_mop.account_data WHERE id = '%u'", player->GetSession()->GetAccountId());

		if (!select) // Just in case, but should not happen
		{
			player->GetSession()->SendAreaTriggerMessage("Something went wrong, please contact the administrator about your issue!");
			player->CLOSE_GOSSIP_MENU();
			return false;
		}

		Field* fields = select->Fetch();
		uint32 gold = fields[3].GetUInt32();

		return gold;
	}

	uint32 SelectSilver(Player* player)
	{
		QueryResult select = LoginDatabase.PQuery("SELECT silver FROM cms_mop.account_data WHERE id = '%u'", player->GetSession()->GetAccountId());

		if (!select) // Just in case, but should not happen
		{
			player->GetSession()->SendAreaTriggerMessage("Something went wrong, please contact the administrator about your issue!");
			player->CLOSE_GOSSIP_MENU();
			return false;
		}

		Field* fields = select->Fetch();
		uint32 silver = fields[2].GetUInt32();

		return silver;
	}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		if (player->IsInCombat())
		{
			player->GetSession()->SendNotification("You're in combat!");
			player->CLOSE_GOSSIP_MENU();
			return false;
		}

		std::stringstream points;
		points << "My Gold: " << SelectGold(player) << "\n";
		points << "My Silver: " << SelectSilver(player) << "\n";

		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "I want to exchange my Gold![Under Construction]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "I want to exchange my Silver![Under Construction]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 10);
		player->SEND_GOSSIP_MENU(1, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();

		if (!player)
			return false;

		uint32 gold = SelectGold(player);
		uint32 silver = SelectSilver(player);

		// If player is not owner and donate points are higher than 25 to prevent any hacks
		if (player->GetSession()->GetSecurity() < 5 && gold > 50)
		{
			player->GetSession()->SendAreaTriggerMessage("You have large amount of Gold: %u. This is probably a website related problem, please immediately contact the administrator via ticket about this issue.", gold);
			player->CLOSE_GOSSIP_MENU();
			return false;
		}

		switch (action)
		{
		case GOSSIP_ACTION_INFO_DEF + 2:
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "I want to exchange 1 gold for 10 apexis shards!", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3); // second menu
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1337);
			player->SEND_GOSSIP_MENU(1, creature->GetGUID());
			break;

		case GOSSIP_ACTION_INFO_DEF + 3:
			if (gold < 1)
			{
				player->GetSession()->SendAreaTriggerMessage("You don't have the required Gold to make this transcation.");
				player->CLOSE_GOSSIP_MENU();

			}
			else
			{
				LoginDatabase.PExecute("UPDATE cms_mop.account_data SET gold = '%u' -1 WHERE id = '%u'", gold, player->GetSession()->GetAccountId());
				creature->MonsterWhisper("Successfully brought 10 Apexis Shard for 1 Gold!", LANG_UNIVERSAL, player->GetGUID());
				player->AddItem(APEXIS_SHARD, 10);
				player->SaveToDB();
				player->CLOSE_GOSSIP_MENU();
			}
			break;

		case GOSSIP_ACTION_INFO_DEF + 1337:
			OnGossipHello(player, creature); // main menu
			player->SEND_GOSSIP_MENU(1, creature->GetGUID());
			break;
		}
		return true;
	}
};

void AddSC_Donation_NPC()
{
	new Donation_NPC;
}