#include "ScriptPCH.h"
// BY AEGWYNN DEVELOPERS FOR AEGWYNN

class System_OnKill : public PlayerScript
{
public:
	System_OnKill() : PlayerScript("System_OnKill") {}

	void OnPVPKill(Player * Killer, Player * Victim)
	{
		if (!Killer->InBattleground())
			return;
		if (!Victim->InBattleground())
			return;
		uint32 KillerGUID = Killer->GetGUIDLow();
		uint32 VictimGUID = Victim->GetGUIDLow();

		

		struct KillStreak_Info
		{
			uint32 killstreak;
			uint32 lastkill;
		};

		static std::map<uint32, KillStreak_Info> KillStreakData;

		if (KillerGUID == VictimGUID || KillStreakData[KillerGUID].lastkill == VictimGUID)
			return;

		if (KillStreakData[VictimGUID].killstreak >= 3)
		{
			std::ostringstream ss;
			ss << "|cFF81CF42" << Killer->GetName() << "|r has stopped |cFFFFFFFF" << Victim->GetName() << " killstreak! ";
			sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
		}

		++KillStreakData[KillerGUID].killstreak;
		KillStreakData[KillerGUID].lastkill = VictimGUID;
		KillStreakData[VictimGUID].killstreak = 0;
		KillStreakData[VictimGUID].lastkill = 0;

		if (KillStreakData[KillerGUID].killstreak % 10 == 0) // send killstreak message every 10 kills
		{
			std::ostringstream ss;
			ss << "|cFF81CF42" << Killer->GetName() << "|r is leading the charge with |cFF42A0CF" << KillStreakData[KillerGUID].killstreak << "|r kills!";
			sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
		}
		if (KillStreakData[KillerGUID].killstreak % 15 == 0) // send killstreak message every 10 kills
		{
			std::ostringstream ss;
			ss << "|cFF81CF42" << Killer->GetName() << "|r is DOMINATING with |cFF42A0CF" << KillStreakData[KillerGUID].killstreak << "|r kills!";
			sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
		}
		if (KillStreakData[KillerGUID].killstreak % 20 == 0) // send killstreak message every 10 kills
		{
			std::ostringstream ss;
			ss << "|cFF81CF42" << Killer->GetName() << "|r is on a MEGA-KILL streak with |cFF42A0CF" << KillStreakData[KillerGUID].killstreak << "|r kills!";
			sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
		}
		if (KillStreakData[KillerGUID].killstreak % 25 == 0) // send killstreak message every 10 kills
		{
			std::ostringstream ss;
			ss << "|cFF81CF42" << Killer->GetName() << "|r is Godlike with |cFF42A0CF" << KillStreakData[KillerGUID].killstreak << "|r Kills!";
			sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
		}
		if (KillStreakData[KillerGUID].killstreak % 30 == 0) // send killstreak message every 10 kills
		{
			std::ostringstream ss;
			ss << "|cFF81CF42" << Killer->GetName() << "|r is on a RAMPAGE with |cFF42A0CF" << KillStreakData[KillerGUID].killstreak << "|r kills!";
			sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
		}

		else if (KillStreakData[KillerGUID].killstreak == 3)
		{
			std::ostringstream ss;
			ss << "|cFF81CF42" << Killer->GetName() << "|r has drew first blood!";
			sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
		}

	}
};

void AddSC_PvP_System()
{
	new System_OnKill;
}