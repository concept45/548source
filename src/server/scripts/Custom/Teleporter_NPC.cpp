#include "ScriptPCH.h"

 class Teleporter_NPC : public CreatureScript
{
public:
	Teleporter_NPC() : CreatureScript("Teleporter_NPC") {}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		if (player->IsInCombat())
		{
			creature->MonsterSay("You're in combat!", LANG_UNIVERSAL, player->GetGUID());
			player->PlayerTalkClass->SendCloseGossip();
				return false;
		}
		if (player->GetSession()->GetSecurity() >= 2)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "MVP Mall", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1992);
		if (player->GetSession()->GetSecurity() >= 1)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Vip Mall", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1991);

		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Mall", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Cities", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 999);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Transmogrification Zone", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Professions Area", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 6);
		player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE, "Duel Zone", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3, "Are you sure you want to travel to Duel Zone?", 0, false);
		player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE, "Gurubashi Arena", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 5, "Are you sure you want to travel to Gurubashi Arena? You will be flagged for pvp.", 0, false);
		//player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "World Bosses", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);
		//player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Raids", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 444);
		
		
		player->SEND_GOSSIP_MENU(1, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();

		if (!player)
			return false;

		switch (action)
		{

			// Mall
		case GOSSIP_ACTION_INFO_DEF + 1:
			player->TeleportTo(870, 4332.155273f, 2805.197754f, 54.559120f, 2.488806f);
			break;

			// Vip Mall
		case GOSSIP_ACTION_INFO_DEF + 1991:
			player->TeleportTo(530, -2720.421387f, 8306.852539f, -83.080750f, 4.727089f);
			break;

			// Vip+ Mall
		case GOSSIP_ACTION_INFO_DEF + 1992:
			player->TeleportTo(530, -2806.357910f, 8335.475586f, -94.051811f, 3.153113f);
			break;

		case GOSSIP_ACTION_INFO_DEF + 999: // cites
			player->PlayerTalkClass->ClearMenus();
			switch (player->GetTeam())
			{
			case ALLIANCE:
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Stormwind", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 111);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Iron Forge", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 112);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Darnassus", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 113);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Exodar", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 114);
				break;

			case HORDE:
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Orgrimmar", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 222);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "ThunderBuff", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 223);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "UnderCity", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 224);
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Silvermoon", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 225);
				break;

			}
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Dalaran", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 333);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 55);
			player->SEND_GOSSIP_MENU(1, creature->GetGUID());
			break;

		case GOSSIP_ACTION_INFO_DEF + 444:
			player->PlayerTalkClass->ClearMenus();
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Throne Of Thunder", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 555); // Throne Of Thunder
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Terrace of Endless Springs", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 556); // Terrace of Endless Springs
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Mogu'Shan Palace", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 557); // Mogu'Shan Palace
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Mogu'Shan Vaults", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 558); // Mogu'Shan Vaults
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 55);
			player->SEND_GOSSIP_MENU(1, creature->GetGUID());
			break;

		case GOSSIP_ACTION_INFO_DEF + 555:
			player->TeleportTo(1064, 7241.574219f, 5037.674316f, 76.219528f, 5.485190f);// Throne Of Thunder
			break;

		case GOSSIP_ACTION_INFO_DEF + 556:
			player->TeleportTo(870, 870.677063f, -106.885437f, 460.886536f, 0.895667f);// Terrace of Endless Springs
			break;

		case GOSSIP_ACTION_INFO_DEF + 557:
			player->TeleportTo(870, 1381.751099f, 448.579468f, 478.923126f, 5.450666f);// Mogu'Shan Palace
			break;

		case GOSSIP_ACTION_INFO_DEF + 558:
			player->TeleportTo(870, 3998.489258f, 1090.698242f, 497.154724f, 2.2545491f);// Mogu'Shan Vaults
			break;

		case GOSSIP_ACTION_INFO_DEF + 2:
			player->TeleportTo(870, 4298.963379f, 2710.410889f, 84.144890f, 4.067532f);
			break;
			// Duel Zone
		case GOSSIP_ACTION_INFO_DEF + 3:
			player->TeleportTo(870, 3848.672852f, 1700.082031f, 932.670532f, 6.001708f);
			break;
			// Gurubashi Arena (PvP)
		case GOSSIP_ACTION_INFO_DEF + 5:
			player->TeleportTo(0, -13245.786133f, 194.051025f, 30.997446f, 1.075586f);
			break;
			// Professions Zone)
		case GOSSIP_ACTION_INFO_DEF + 6:
			player->TeleportTo(870, 4446.328613f, 2917.211670f, 89.88985f, 0.941552f);
			break;
			
			// Second Menu
		case GOSSIP_ACTION_INFO_DEF + 4:
			player->PlayerTalkClass->ClearMenus();

			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE, "Galleon [Easy]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 11, "Are you sure travel to Galleon terriority?", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE, "Sha of Anger [Medium]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 22, "Are you sure to travel to Sha of Anger terriority?", 0, false);
			player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE, "Fel Lord [Hard]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 33, "Are you sure to travel to Fel Lord terriority?", 0, false);
			//player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE, "Nalak [Hard]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 44, "Are you sure to travel to Nalak terriority?", 0, false);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Back", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 55);
			player->SEND_GOSSIP_MENU(1, creature->GetGUID());
			break;

			// Teleportation
		case GOSSIP_ACTION_INFO_DEF + 11:
			player->TeleportTo(870, -515.946960f, -178.860168f, 156.639816f, 2.228768f); // Galleon
			break;
		
		case GOSSIP_ACTION_INFO_DEF + 22:
			player->TeleportTo(870, 2432.875244f, 642.097961f, 501.001617f, 3.706324f); // Sha of Anger
			break;
		
		case GOSSIP_ACTION_INFO_DEF + 33:
			player->TeleportTo(1, 4067.401123f, -2502.630859f, 1038.441284f, 6.197123f); // Fel Lord
			break;

		//case GOSSIP_ACTION_INFO_DEF + 44:
			//player->TeleportTo(1, 6630.984863f, 5648.854004f, 12.870471f, 5.526936f); // Nalak
			//break;

		case GOSSIP_ACTION_INFO_DEF + 111:
			player->TeleportTo(0, -8828.082031f, 624.667847f, 94.254097f, 94.254097f); // StormWind
			break;

		case GOSSIP_ACTION_INFO_DEF + 112:
			player->TeleportTo(0, -4927.439453f, -947.779114f, 501.577484f, 5.423470f); // Iron Forge
			break;

		case GOSSIP_ACTION_INFO_DEF + 113:
			player->TeleportTo(1, 9952.024414f, 2280.216553f, 1341.391846f, 1.580162f); // Darnassus
			break;

		case GOSSIP_ACTION_INFO_DEF + 114:
			player->TeleportTo(530, -3971.947021f, -11642.891602f, -138.890610f, -138.890610f); // Exodar
			break;

		case GOSSIP_ACTION_INFO_DEF + 222:
			player->TeleportTo(1, 1563.988525f, -4402.024902f, 16.581837f, 0.158174f); // Orgrimmar
			break;

		case GOSSIP_ACTION_INFO_DEF + 223:
			player->TeleportTo(1, -1264.667969f, 76.281921f, 127.916634f, 4.401988f); // Thunder buff
			break;

		case GOSSIP_ACTION_INFO_DEF + 224:
			player->TeleportTo(0, 1569.706177f, 240.513962f, -44.856110f, -44.856110f); // Undercity
			break;

		case GOSSIP_ACTION_INFO_DEF + 225:
			player->TeleportTo(530, 9503.443359f, -6824.226563f, 16.492182f, 3.947149f); // Silver moon
			break;

		case GOSSIP_ACTION_INFO_DEF + 333:
			player->TeleportTo(571, 5803.832031f, 626.903442f, 647.458740f, 1.930594f); // Dalaran
			break;

		case GOSSIP_ACTION_INFO_DEF + 55:
			OnGossipHello(player, creature); // main menu
			player->SEND_GOSSIP_MENU(1, creature->GetGUID());
			break;
		}
		return true;
	}
 };

 void AddSC_Teleporter_NPC()
 {
	 new Teleporter_NPC;
 }