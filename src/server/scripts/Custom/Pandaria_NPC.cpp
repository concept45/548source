#include "ScriptPCH.h"

class Pandaria_NPC : public CreatureScript
{
public:
	Pandaria_NPC() : CreatureScript("Pandaria_NPC") {}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		if (player->getRace() != RACE_PANDAREN_NEUTRAL)
		{
			creature->MonsterWhisper("You're already apart of a faction!", LANG_UNIVERSAL, player->GetGUID());
			player->PlayerTalkClass->SendCloseGossip();
			return false;
		}

		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "I want to choose my destiny!", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
		player->SEND_GOSSIP_MENU(1, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();

		if (!player)
			return false;

		switch (action)
		{

		case GOSSIP_ACTION_INFO_DEF + 1:
		{
			player->setFaction(469);
			break;

		}

		}

		return true;
	}
};

void AddSC_Pandaria_NPC()
{
	new Pandaria_NPC;
}