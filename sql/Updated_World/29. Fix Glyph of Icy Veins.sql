-- Glyph of Icy Veins 5.4.1
DELETE FROM `spell_script_names` WHERE `spell_id` IN(131078,31707);
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES
(131078,"spell_mage_glyph_of_icy_veins"),
(31707 ,"spell_mage_waterbolt");
DELETE FROM `spell_script_names` WHERE `spell_id`=44614 AND `ScriptName`="spell_mage_frostfire_bolt";
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES
(44614 ,"spell_mage_frostfire_bolt");