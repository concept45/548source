-- First period boss etc scriptnames a few
UPDATE CREATURE_TEMPLATE SET `ScriptName` = 'mob_sha_puddle' WHERE ENTRY = 71603;
UPDATE CREATURE_TEMPLATE SET `ScriptName` = 'mob_contaminated_puddle' WHERE ENTRY = 71604;
UPDATE CREATURE_TEMPLATE SET `ScriptName` = 'boss_immerseus' WHERE ENTRY = 71543;

-- This fixes Aqueous Defender
DELETE FROM `creature_template` WHERE ENTRY = 73191;
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `difficulty_entry_4`, `difficulty_entry_5`, `difficulty_entry_6`, `difficulty_entry_7`, `difficulty_entry_8`, `difficulty_entry_9`, `difficulty_entry_10`, `difficulty_entry_11`, `difficulty_entry_12`, `difficulty_entry_13`, `difficulty_entry_14`, `difficulty_entry_15`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `exp_unk`, `faction_A`, `faction_H`, `npcflag`, `npcflag2`, `speed_walk`, `speed_run`, `speed_fly`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `type_flags2`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Mana_mod_extra`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (73191, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37415, 0, 0, 0, 'Aqueous Defender', NULL, NULL, 0, 90, 90, 4, 0, 14, 14, 0, 0, 1, 1.14286, 1.14286, 1, 1, 10000, 28000, 0, 0, 1, 2000, 2000, 1, 0, 2048, 0, 0, 0, 00000000, 0, 0, 0, 0, 0, 4, 2097224, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'SmartAI', 0, 3, 1, 45, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, '', 18414);

DELETE FROM `smart_scripts` WHERE entryorguid = 73191;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES (73191, 0, 0, 0, 0, 0, 88, 0, 15000, 40000, 15000, 40000, 11, 147185, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 'Cast Rushing Waters every 15 - 40');

-- This fixes Tormented Initiate
UPDATE creature_template set `faction_a` = 14, `faction_h`= 14, `lootid` = 75000 where entry = 73349;

DELETE FROM creature_text where entry = 73349;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES (73349, 0, 0, 'Defend the pools!', 14, 0, 100, 0, 0, 0, 'SoO - Tormented Initiate');
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES (73349, 0, 1, 'Our work is not yet complete! Stop them!', 14, 0, 100, 0, 0, 0, 'SoO - Tormented Initiate');
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `comment`) VALUES (73349, 0, 2, 'We must defend the pools from their corruption!', 14, 0, 100, 0, 0, 0, 'SoO - Tormented Initiate');

-- some puddles
UPDATE creature_template set `faction_a` = 14, `faction_h`= 14 where entry = 71603;
UPDATE creature_template set `faction_a` = 14, `faction_h`= 14 where entry = 71604;

-- immerseus
UPDATE creature_template set `faction_a` = 14, `faction_h`= 14 where entry = 71543;
UPDATE creature_template set unit_flags = 4 WHERE entry = 71543;
