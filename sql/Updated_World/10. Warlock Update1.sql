UPDATE `spell_proc_event` SET `SpellFamilyMask0`=1000 WHERE (`entry`=117896);
REPLACE INTO `spell_script_names` VALUES (117828, 'spell_warlock_backdraft_aura');
REPLACE INTO `spell_script_names` VALUES (117896, 'spell_warlock_backdraft_proc');
REPLACE INTO `spell_script_names` VALUES (116858, 'spell_warl_chaos_bolt');
DELETE FROM `spell_script_names` WHERE `spell_id`=29722 AND `ScriptName`='spell_warl_incinerate';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (29722, 'spell_warl_incinerate');
