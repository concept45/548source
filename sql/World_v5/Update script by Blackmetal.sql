DELETE FROM disables WHERE sourceType=2 AND entry=568; -- Open Zul'Aman
DELETE FROM disables WHERE sourceType=2 AND entry=580; -- Open Sunwell
DELETE FROM disables WHERE sourceType=2 AND entry=631; -- Open ICC
DELETE FROM disables WHERE sourceType=2 AND entry=649; -- Open Trial of the Crusader
DELETE FROM disables WHERE sourceType=2 AND entry=615; -- Open The Obsidian Sanctum

-- Warlock, fix Mastery: Emberstorm's Shadowburn bonus
DELETE FROM `spell_script_names` WHERE `spell_id`=17877 AND `ScriptName`='spell_warl_shadowburn_damage';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (17877, 'spell_warl_shadowburn_damage');

-- Hack fix temp Divine Purpose 
REPLACE into `spell_proc_event` (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `SpellFamilyMask3`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) values
('86172','0','10','8388608','2228354','40960','0','87376','1027','0','25','1');

-- Make NPC Cast spellable in Metheridon Lair
UPDATE smart_scripts SET event_flags=0 WHERE entryorguid=18829;

-- Update Spell Hunter - Cobra Strikes
UPDATE spell_proc_event SET SpellFamilyMask1=8388609,procEx=0,CustomChance=15 WHERE entry=53260;

-- Delete unsued GO things
DELETE FROM gameobject WHERE guid IN (151162,211867);

-- Fix quest the-keymaster
UPDATE `quest_template` SET `SpecialFlags` = 0, `RequiredSpellCast1` = 0 WHERE `ID` = 10256;

-- Script for Legendary Cloaks
REPLACE INTO `spell_bonus_data` (`entry`, `ap_bonus`, `comments`) VALUES  ('149276', '0.2', 'Flurry of Xuen'),('147891', '0.2', 'Flurry of Xuen');
DELETE FROM spell_script_names WHERE spell_id IN (146193,146194,146198,148008);
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES ('146193', 'spell_item_endurance_of_niuzao'),('146194', 'spell_item_flurry_of_xuen'),('146198', 'spell_item_essence_of_yulon'),('148008', 'spell_item_essence_of_yulon_dmg');

-- Insert missing Epic Wp Boss Wing Leader Ner'onok
DELETE FROM creature_loot_template WHERE entry=362205 AND item=87547;
INSERT INTO `creature_loot_template` VALUES ('362205', '87547', '0', '1', '1', '1', '1');


------------------ IMPORT UPDATE FROM QUADRAL ------------------------------------
DELETE FROM creature_template WHERE entry=190001;
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `difficulty_entry_4`, `difficulty_entry_5`, `difficulty_entry_6`, `difficulty_entry_7`, `difficulty_entry_8`, `difficulty_entry_9`, `difficulty_entry_10`, `difficulty_entry_11`, `difficulty_entry_12`, `difficulty_entry_13`, `difficulty_entry_14`, `difficulty_entry_15`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `exp_unk`, `faction_A`, `faction_H`, `npcflag`, `npcflag2`, `speed_walk`, `speed_run`, `speed_fly`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `type_flags2`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Mana_mod_extra`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES 
(190001, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5564, 0, 0, 0, 'Xuen', '', '', 8345, 86, 86, 1, 0, 35, 35, 1, 0, 2, 1.42857, 1.14286, 1, 1, 252, 357, 0, 304, 7.5, 3000, 2000, 1, 0, 0, 8, 0, 0, 00000000, 0, 0, 215, 320, 44, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37146, 30284, 37427, 37432, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 3, 1, 11.4515, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 151, 0, 0, 1, 2, 'Pandaria_NPC', 16048);

DELETE FROM spell_script_names WHERE spell_id=126135 AND ScriptName='spell_pri_lightwell_Summon';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (126135, 'spell_pri_lightwell_Summon');


DELETE FROM creature_template WHERE entry=190002;
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `difficulty_entry_4`, `difficulty_entry_5`, `difficulty_entry_6`, `difficulty_entry_7`, `difficulty_entry_8`, `difficulty_entry_9`, `difficulty_entry_10`, `difficulty_entry_11`, `difficulty_entry_12`, `difficulty_entry_13`, `difficulty_entry_14`, `difficulty_entry_15`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `exp_unk`, `faction_A`, `faction_H`, `npcflag`, `npcflag2`, `speed_walk`, `speed_run`, `speed_fly`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `type_flags2`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Mana_mod_extra`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES 
(190002, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5564, 0, 0, 0, 'Teleporter', '', '', 8345, 86, 86, 1, 0, 35, 35, 1, 0, 2, 1.42857, 1.14286, 1, 1, 252, 357, 0, 304, 7.5, 3000, 2000, 1, 0, 0, 8, 0, 0, 00000000, 0, 0, 215, 320, 44, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37146, 30284, 37427, 37432, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 3, 1, 11.4515, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 151, 0, 0, 1, 2, 'Teleporter_NPC', 16048);

DELETE FROM creature_template WHERE entry=190003;
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `difficulty_entry_4`, `difficulty_entry_5`, `difficulty_entry_6`, `difficulty_entry_7`, `difficulty_entry_8`, `difficulty_entry_9`, `difficulty_entry_10`, `difficulty_entry_11`, `difficulty_entry_12`, `difficulty_entry_13`, `difficulty_entry_14`, `difficulty_entry_15`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `exp_unk`, `faction_A`, `faction_H`, `npcflag`, `npcflag2`, `speed_walk`, `speed_run`, `speed_fly`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `type_flags2`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Mana_mod_extra`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES 
(190003, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5564, 0, 0, 0, 'Donation', '', '', 8345, 86, 86, 1, 0, 35, 35, 1, 0, 2, 1.42857, 1.14286, 1, 1, 252, 357, 0, 304, 7.5, 3000, 2000, 1, 0, 0, 8, 0, 0, 00000000, 0, 0, 215, 320, 44, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37146, 30284, 37427, 37432, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 3, 1, 11.4515, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 151, 0, 0, 1, 2, 'Donation_NPC', 16048);

DELETE FROM creature_template WHERE entry=93123;
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `difficulty_entry_4`, `difficulty_entry_5`, `difficulty_entry_6`, `difficulty_entry_7`, `difficulty_entry_8`, `difficulty_entry_9`, `difficulty_entry_10`, `difficulty_entry_11`, `difficulty_entry_12`, `difficulty_entry_13`, `difficulty_entry_14`, `difficulty_entry_15`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `exp_unk`, `faction_A`, `faction_H`, `npcflag`, `npcflag2`, `speed_walk`, `speed_run`, `speed_fly`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `type_flags2`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Mana_mod_extra`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES 
(93123, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5564, 0, 0, 0, 'Trigger Warsong', '', '', 8345, 86, 86, 1, 0, 35, 35, 1, 0, 2, 1.42857, 1.14286, 1, 1, 252, 357, 0, 304, 7.5, 3000, 2000, 1, 0, 0, 8, 0, 0, 00000000, 0, 0, 215, 320, 44, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37146, 30284, 37427, 37432, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 3, 1, 11.4515, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 151, 0, 0, 1, 2, 'AreaTrigger_at_Tele_Trigger', 16048);

DELETE FROM spell_script_names WHERE spell_id=51160 AND ScriptName='spell_dk_plague_strike';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (51160, 'spell_dk_plague_strike');

DELETE FROM creature_template WHERE entry IN (911343,911344);
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `difficulty_entry_4`, `difficulty_entry_5`, `difficulty_entry_6`, `difficulty_entry_7`, `difficulty_entry_8`, `difficulty_entry_9`, `difficulty_entry_10`, `difficulty_entry_11`, `difficulty_entry_12`, `difficulty_entry_13`, `difficulty_entry_14`, `difficulty_entry_15`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `exp_unk`, `faction_A`, `faction_H`, `npcflag`, `npcflag2`, `speed_walk`, `speed_run`, `speed_fly`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `type_flags2`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Mana_mod_extra`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES 
(911343, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 108, 0, 0, 0, 'Morpher', 'Aegwynn', '', 0, 90, 90, 0, 0, 35, 35, 1, 0, 1.2, 1.14286, 1.14286, 1.2, 0, 2, 2, 0, 24, 1, 2000, 0, 1, 0, 2048, 8, 0, 0, 00000000, 0, 0, 1, 1, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'SmartAI', 0, 3, 1, 25, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 'npc_morph_first', 15595),
(911344, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 108, 0, 0, 0, 'Morpher', 'Aegwynn', '', 0, 90, 90, 0, 0, 35, 35, 1, 0, 1.2, 1.14286, 1.14286, 1.2, 0, 2, 2, 0, 24, 1, 2000, 0, 1, 0, 2048, 8, 0, 0, 00000000, 0, 0, 1, 1, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'SmartAI', 0, 3, 1, 25, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 'npc_morph_second', 15595);



------------------ END OF QUADRAL SCRIPT ------------------------