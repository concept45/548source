-- Galaron
DELETE FROM spell_script_names WHERE spell_id=122735 AND ScriptName='spell_garalon_furious_swipe';
INSERT INTO spell_script_names VALUES (122735,'spell_garalon_furious_swipe');

DELETE FROM spell_script_names WHERE spell_id=123808 AND ScriptName='spell_garalon_pheromones_forcecast';
INSERT INTO spell_script_names VALUES (123808,'spell_garalon_pheromones_forcecast');

DELETE FROM spell_script_names WHERE spell_id=123495 AND ScriptName='spell_garalon_mend_leg';
INSERT INTO spell_script_names VALUES (123495,'spell_garalon_mend_leg');

DELETE FROM spell_script_names WHERE spell_id=117709 AND ScriptName='spell_garalon_crush_trigger';
INSERT INTO spell_script_names VALUES (117709,'spell_garalon_crush_trigger');

DELETE FROM spell_script_names WHERE spell_id=123109 AND ScriptName='spell_garalon_pheromones_taunt';
INSERT INTO spell_script_names VALUES (123109,'spell_garalon_pheromones_taunt');

DELETE FROM spell_script_names WHERE spell_id=122786 AND ScriptName='spell_garalon_broken_leg';
INSERT INTO spell_script_names VALUES (122786,'spell_garalon_broken_leg');

DELETE FROM spell_script_names WHERE spell_id=123818 AND ScriptName='spell_garalon_damaged';
INSERT INTO spell_script_names VALUES (123818,'spell_garalon_damaged');

DELETE FROM spell_script_names WHERE spell_id=128573 AND ScriptName='spell_garalon_pheromones_summon';
INSERT INTO spell_script_names VALUES (128573,'spell_garalon_pheromones_summon');

DELETE FROM spell_script_names WHERE spell_id=123120 AND ScriptName='spell_garalon_pheromones_trail_dmg';
INSERT INTO spell_script_names VALUES (123120,'spell_garalon_pheromones_trail_dmg');

DELETE FROM spell_script_names WHERE spell_id=123100 AND ScriptName='spell_garalon_pheromones_switch';
INSERT INTO spell_script_names VALUES (123100,'spell_garalon_pheromones_switch');

-- Meljarak
DELETE FROM spell_script_names WHERE spell_id=122064 AND ScriptName='spell_meljarak_corrosive_resin';
INSERT INTO spell_script_names VALUES (122064,'spell_meljarak_corrosive_resin');

DELETE FROM spell_script_names WHERE spell_id=122147 AND ScriptName='spell_mending';
INSERT INTO spell_script_names VALUES (122147,'spell_mending');

-- Tayak

DELETE FROM spell_script_names WHERE spell_id=123459 AND ScriptName='spell_tayak_wind_step';
INSERT INTO spell_script_names VALUES (123459,'spell_tayak_wind_step');

DELETE FROM spell_script_names WHERE spell_id=122982 AND ScriptName='spell_unseen_strike';
INSERT INTO spell_script_names VALUES (122982,'spell_unseen_strike');

DELETE FROM spell_script_names WHERE spell_id=122994 AND ScriptName='spell_unseen_strike_dmg';
INSERT INTO spell_script_names VALUES (122994,'spell_unseen_strike_dmg');

DELETE FROM spell_script_names WHERE spell_id=124258 AND ScriptName='spell_tayak_storms_vehicle';
INSERT INTO spell_script_names VALUES (124258,'spell_tayak_storms_vehicle');

DELETE FROM spell_script_names WHERE spell_id=124783 AND ScriptName='spell_tayak_storm_unleashed_dmg';
INSERT INTO spell_script_names VALUES (124783,'spell_tayak_storm_unleashed_dmg');

DELETE FROM spell_script_names WHERE spell_id=122853 AND ScriptName='spell_tempest_slash';
INSERT INTO spell_script_names VALUES (122853,'spell_tempest_slash');

DELETE FROM spell_script_names WHERE spell_id=123814 AND ScriptName='spell_tayak_su_visual';
INSERT INTO spell_script_names VALUES (123814,'spell_tayak_su_visual');

DELETE FROM spell_script_names WHERE spell_id=124024 AND ScriptName='spell_su_dummy_visual';
INSERT INTO spell_script_names VALUES (124024,'spell_su_dummy_visual');

DELETE FROM spell_script_names WHERE spell_id=123633 AND ScriptName='spell_gale_winds';
INSERT INTO spell_script_names VALUES (123633,'spell_gale_winds');

DELETE FROM spell_script_names WHERE spell_id=123600 AND ScriptName='spell_su_dummy';
INSERT INTO spell_script_names VALUES (123600,'spell_su_dummy');

DELETE FROM spell_script_names WHERE spell_id=123616 AND ScriptName='spell_su_dumaura';
INSERT INTO spell_script_names VALUES (123616,'spell_su_dumaura');

-- Zorlok

DELETE FROM spell_script_names WHERE spell_id=122852 AND ScriptName='spell_inhale';
INSERT INTO spell_script_names VALUES (122852,'spell_inhale');

DELETE FROM spell_script_names WHERE spell_id=122440 AND ScriptName='spell_attenuation';
INSERT INTO spell_script_names VALUES (122440,'spell_attenuation');

DELETE FROM spell_script_names WHERE spell_id=122718 AND ScriptName='spell_force_verve';
INSERT INTO spell_script_names VALUES (122718,'spell_force_verve');

DELETE FROM spell_script_names WHERE spell_id=122336 AND ScriptName='spell_sonic_ring';
INSERT INTO spell_script_names VALUES (122336,'spell_sonic_ring');

DELETE FROM spell_script_names WHERE spell_id=124018 AND ScriptName='spell_pheromones_of_zeal';
INSERT INTO spell_script_names VALUES (124018,'spell_pheromones_of_zeal');

DELETE FROM spell_script_names WHERE spell_id=122761 AND ScriptName='spell_zorlok_exhale';
INSERT INTO spell_script_names VALUES (122761,'spell_zorlok_exhale');

DELETE FROM spell_script_names WHERE spell_id=122760 AND ScriptName='spell_zorlok_exhale_damage';
INSERT INTO spell_script_names VALUES (122760,'spell_zorlok_exhale_damage');

DELETE FROM spell_script_names WHERE spell_id=122740 AND ScriptName='spell_convert';
INSERT INTO spell_script_names VALUES (122740,'spell_convert');

-- General
DELETE FROM spell_script_names WHERE spell_id=123421 AND ScriptName='spell_vital_strikes';
INSERT INTO spell_script_names VALUES (123421,'spell_vital_strikes');
