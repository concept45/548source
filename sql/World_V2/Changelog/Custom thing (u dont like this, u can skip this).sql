-- First boss stoneguards
UPDATE creature_template SET dmg_multiplier=1,mindmg=40000,maxdmg=50000 WHERE entry IN (60043,60047,60051,59915);
-- Feng the Accursed
UPDATE creature_template SET dmg_multiplier=1.5,mindmg=80000,maxdmg=100000 WHERE entry=60009;
-- Emperor Rage
UPDATE creature_template SET dmg_multiplier=1,mindmg=60000,maxdmg=80000 WHERE entry=60396;
-- Emperor Courage
UPDATE creature_template SET dmg_multiplier=1,mindmg=60000,maxdmg=80000 WHERE entry=60398;
-- Emperor Strength
UPDATE creature_template SET dmg_multiplier=1,mindmg=100000,maxdmg=120000 WHERE entry=60397;
-- Mogushan Secret Keeper
UPDATE creature_template SET dmg_multiplier=1,mindmg=40000,maxdmg=50000 WHERE entry=61131;
-- Jan-xi
UPDATE creature_template SET dmg_multiplier=2,mindmg=60000,maxdmg=80000 WHERE entry=60400;
DELETE FROM creature_loot_template WHERE entry=60399;
UPDATE creature_template SET lootid=0,mingold=0,maxgold=0 WHERE entry=60399;
-- Boss 4 MSV
UPDATE creature_template SET dmg_multiplier=1.5,mindmg=80000,maxdmg=100000 WHERE entry in (60709,60710,60701);
-- Remove NPC unkillable
DELETE FROM creature WHERE id IN (61334,61989);