-- but npcflag does not include UNIT_NPC_FLAG_QUESTGIVER
UPDATE creature_template SET npcflag=npcflag+2 WHERE entry IN (620,14871,20912,35627,39679);
UPDATE creature_template SET npcflag=npcflag+2 WHERE entry IN (39698,39700,39705,42333,43778,47626,49874,50039,66220);
