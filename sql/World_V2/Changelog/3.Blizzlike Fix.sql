-- Use Creature GUID from 8725 -> 9400 --  
SET @ENTRY := 8725;
-------- Respawn Zone ----------
-- Respawn NPC Raider Bork <War Mount Quartermaster>
delete from creature where id=12796;
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY,'12796','1','0','0','1','1','0','12796','1637.85','-4233.41','52.0885','4.57838','300','0','0','10456','0','0','0','0','134218496','0','0','0',NULL);
-- Respawn Zandalari Warscout
delete from creature where id=69768;
-- Respawn time changed to 8hours duo low population
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+1,'69768','870','0','0','1','1','0','69768','-296.333','4159.31','159.435','3.49205','28800','0','0','5297916','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+2,'69768','870','0','0','1','1','0','69768','1119.74','4735.72','133.887','0.260127','28800','0','0','5297916','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+3,'69768','870','0','0','1','1','0','69768','2896.54','496.778','513.847','5.39972','28800','0','0','5297916','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+4,'69768','870','0','0','1','1','0','69768','2823.64','-1676.45','257.909','5.80389','28800','0','0','5297916','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+5,'69768','870','0','0','1','1','0','69768','-1399.98','587.054','15.2896','5.61267','28800','0','0','5297916','0','0','0','0','0','2048','0','0',NULL);
-- Add missing NPC Arcanist Firael <Legacy Justice Quartermaster>
delete from creature where id=33964;
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+6,'33964','571','0','0','1','1','0','33964','5762.55','732.519','653.665','2.31466','300','0','0','10080','8814','0','0','0','0','0','0','0',NULL);

-- Required for Random Battleground : 10 player
UPDATE battleground_template SET MinPlayersPerTeam=10 WHERE id=32;

-- Add missing mounts to vendor Old Whitenose <Dragon Turtle Breeder>
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 2, 91007, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 7, 91008, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 6, 91009, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 13, 91011, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 10, 91012, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 12, 91013, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 11, 91014, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (65068, 9, 91015, 0, 0, 0, 1);

-- Add missing Item - Turtlemaster Odai <Dragon Turtle Breeder> Horde
DELETE FROM npc_vendor WHERE entry=66022 AND item IN (91009,91008,91007,91006,91005,91004,82811,87801,87802,87803,87804,87805);
INSERT INTO npc_vendor VALUES
(66022,17,91009,0,0,0,1),
(66022,16,91008,0,0,0,1),
(66022,15,91007,0,0,0,1),
(66022,14,91006,0,0,0,1),
(66022,13,91005,0,0,0,1),
(66022,12,91004,0,0,0,1),
(66022,1,82811,0,0,0,1),
(66022,7,87801,0,0,0,1),
(66022,8,87802,0,0,0,1),
(66022,9,87803,0,0,0,1),
(66022,10,87804,0,0,0,1),
(66022,11,87805,0,0,0,1);

-- Add remaining missing mounts to Gina Mudclaw 58706
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (58706, 0, 89362, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (58706, 0, 89390, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (58706, 0, 89391, 0, 0, 0, 1);




-- Delete duplicate respawn
DELETE FROM creature WHERE guid IN (629921,988159,997226);
-- Add missing item
DELETE FROM npc_vendor WHERE entry=59908 AND item IN (87781,87782,87783);
INSERT INTO npc_vendor VALUES
(59908,0,87781 ,0,0,0,1),
(59908,0,87782 ,0,0,0,1),
(59908,0,87783 ,0,0,0,1);
-- Add missing item
DELETE FROM npc_vendor WHERE entry=58414 AND item IN (79802,85429,85430);
INSERT INTO npc_vendor VALUES
(58414,0,79802 ,0,0,0,1),
(58414,0,85429 ,0,0,0,1),
(58414,0,85430 ,0,0,0,1);
-- Add missing item
DELETE FROM npc_vendor WHERE entry=64595 AND item IN (89305,89306,89307);
INSERT INTO npc_vendor VALUES
(64595,0,89305 ,0,0,0,1),
(64595,0,89306 ,0,0,0,1),
(64595,0,89307 ,0,0,0,1);
-- Add loot for Zandalari Warscout (69768)
UPDATE creature_template SET lootid=69768 WHERE entry=69768;
DELETE FROM creature_loot_template WHERE entry=69768;
INSERT INTO creature_loot_template VALUES
(69768,94159,30,1,1,1,1),
(69768,94225,16,1,1,1,1),
(69768,94227,16,1,1,1,1),
(69768,94226,16,1,1,1,1),
(69768,94223,16,1,1,1,1),
(69768,94158,6,1,1,1,1);

-- Fixed Dancing Steel Proc
DELETE FROM spell_proc_event WHERE entry=118333;
INSERT INTO spell_proc_event VALUES
(118333,1,0,0,0,0,0,(4+64),0,1,0,30); 
-- Remove Zen Alchemist Stone from disables
DELETE FROM disables WHERE entry=136197 AND sourceType=0;
-- Fixed Zen Alchemist Stone Proc
DELETE FROM spell_proc_event WHERE entry=105574;
INSERT INTO spell_proc_event VALUES
(105574,10,0,0,0,0,0,(16384+262144),0,1,0,35);


-- Group 1005 is unused

--  +10% Attack Power: Group 1003
DELETE FROM spell_group WHERE id=1003 AND spell_id IN (19506,57330);
INSERT INTO spell_group VALUES
(1003,19506), -- Trueshot Aura
(1003,57330); -- Horn of Winter
-- Group +5% Strength, Agility, Intellect: Group 3
DELETE FROM spell_group_stack_rules WHERE group_id=3;
INSERT INTO `spell_group_stack_rules` (`group_id`, `stack_rule`) VALUES (3, 1);
DELETE FROM spell_group WHERE spell_id IN (1126,20217,90363,115921) AND id=3;
INSERT INTO spell_group VALUES 
(3,1126), -- Mark of the Wild
(3,20217), -- Blessing of Kings
(3,90363), -- Embrace of the Shale Spider
(3,115921); -- Legacy of the Emperor
-- +10% Stamina: Group 1083
DELETE FROM spell_group WHERE spell_id IN (90364,21562) AND id=1083;
INSERT INTO spell_group VALUES 
(1083,90364), -- Qiraji Fortitude
(1083,21562); -- Power Word: Fortitude
-- +3000 Mastery Rating: Group 1002
DELETE FROM spell_group WHERE spell_id IN (93435,128997,116956) AND id=1002;
INSERT INTO spell_group VALUES
(1002,93435), -- Roar of Courage
(1002,128997), -- Spirit Beast Blessing
(1002,116956);  -- Grace of Air
-- +5% Critical Strike Chance: Group 1023
DELETE FROM spell_group WHERE spell_id IN (17007,90309,126309,24604,1459,116781) AND id=1023;
INSERT INTO spell_group VALUES
(1023,17007), -- Leader of the Pack
(1023,90309), -- Terrifying Roar
(1023,126309), -- Still Water
(1023,24604), -- Furious Howl
(1023,1459), -- Arcane Brilliance
(1023,116781); -- Legacy of the White Tiger
-- +5% Spell Haste: Group 1034
DELETE FROM spell_group WHERE spell_id IN (24907,49868,15473,51470) AND id=1034;
INSERT INTO spell_group VALUES
(1034,24907), -- Moonkin Aura
(1034,49868), -- Mind Quickening
(1034,15473), -- Shadowform
(1034,51470); -- Elemental Oath
-- +10% Spell Power: Group 1007
DELETE FROM spell_group_stack_rules WHERE group_id=1007;
INSERT INTO `spell_group_stack_rules` (`group_id`, `stack_rule`) VALUES (1007, 1);
DELETE FROM spell_group WHERE spell_id IN (126309,1459,77747,109773) AND id=1007;
INSERT INTO spell_group VALUES
(1007,126309), -- Still Water
(1007,1459), -- Arcane Brilliance
(1007,77747), -- Burning Wrath
(1007,109773); -- Dark Intent
-- +10% Melee and Ranged Attack Speed: Group 1020
DELETE FROM spell_group WHERE spell_id IN (55610,128432,128433,113742,30809) AND id=1020;
INSERT INTO spell_group VALUES
(1020,55610), -- Unholy Aura
(1020,128432), -- Cackling Howl
(1020,128433), -- Serpent's Cunning
(1020,113742), -- Swiftblade's Cunning
(1020,30809); -- Unleashed Rage
--------------- Debuff --------------------
-- -10% Physical damage dealt for 30 seconds : Group 1063
DELETE FROM spell_group_stack_rules WHERE group_id=1063;
INSERT INTO `spell_group_stack_rules` (`group_id`, `stack_rule`) VALUES (1063,1);
DELETE FROM spell_group WHERE spell_id IN (115798,81132,106830,77758,50256,24423,121253,53595,8042,109466,6343) AND id=1063;
INSERT INTO spell_group VALUES
(1063,115798), -- Weakened Blows
(1063,81132), -- Scarlet Fever
(1063,106830), -- Thrash
(1063,77758), -- Thrash
(1063,50256), -- Invigorating Roar
(1063,24423), -- Bloody Screech
(1063,121253), -- Keg Smash
(1063,53595), -- Hammer of the Righteous
(1063,8042), -- Earth Shock
(1063,109466), -- Curse of Enfeeblement
(1063,6343); -- Thunder Clap
-- +4% Physical damage taken for 30 seconds: Group 1009
DELETE FROM spell_group WHERE spell_id IN (81326,81328,51160,35290,57386,55749,50518,86346) AND id=1009;
INSERT INTO spell_group VALUES
(1009,81326), --  Physical Vulnerability
(1009,81328), --  Brittle Bones
(1009,51160), -- Ebon Plaguebringer
(1009,35290), -- Indomitable
(1009,57386), -- Wild Strength
(1009,55749), -- Acid Spit
(1009,50518), -- Ravage
(1009,86346); -- Colossus Smash
-- -4% Armor for 30 seconds, stacks 3 times: Group 1016
DELETE FROM spell_group WHERE spell_id IN (113746,770,50498,50285,8647,7386,20243) AND id=1016;
INSERT INTO spell_group VALUES
(1016,113746), -- Weakened Armor
(1016,770), -- Faerie Fire
(1016,50498), -- Tear Armor
(1016,50285), -- Dust Cloud
(1016,8647), -- Expose Armor
(1016,7386), --  Sunder Armor
(1016,20243); -- Devastate
-- +5% Spell damage taken: Group 1017
DELETE FROM spell_group_stack_rules WHERE group_id=1017;
INSERT INTO `spell_group_stack_rules` (`group_id`, `stack_rule`) VALUES (1017,1);
DELETE FROM spell_group WHERE spell_id IN (34889,24844,58410,1490) AND id=1017;
INSERT INTO spell_group VALUES
(1017,34889), -- Fire Breath
(1017,24844), -- Lightning Breath
(1017,58410), -- Master Poisoner
(1017,1490); -- Curse of the Elements
-- Healing reduction: Group 1072
DELETE FROM spell_group_stack_rules WHERE group_id=1072;
INSERT INTO `spell_group_stack_rules` (`group_id`, `stack_rule`) VALUES (1072,1);
DELETE FROM spell_group WHERE spell_id IN (115804,82654,54680,107428,8679,12294,100130) AND id=1072;
INSERT INTO spell_group VALUES
(1072,115804), -- Mortal Wounds
(1072,82654), -- Widow Venom
(1072,54680), -- Monstrous Bite
(1072,107428), -- Rising Sun Kick
(1072,8679), -- Wound Poison
(1072,12294), -- Mortal Strike
(1072,100130); -- Wild Strike
-- Casting speed reduction: Group 1018
DELETE FROM spell_group_stack_rules WHERE group_id=1018;
INSERT INTO `spell_group_stack_rules` (`group_id`, `stack_rule`) VALUES (1018,1);
DELETE FROM spell_group WHERE spell_id IN (73975,50274,31589,58604,5761,109466) AND id=1018;
INSERT INTO spell_group VALUES
(1018,73975), -- Necrotic Strike
(1018,50274), -- Spore Cloud
(1018,31589), -- Slow
(1018,58604), -- Spore Cloud
(1018,5761), -- Mind-numbing Poison
(1018,109466); -- Curse of Enfeeblement



-- Stoneguard- Mogushan Vault 10M Normal
DELETE FROM creature_loot_template WHERE entry=60051;
UPDATE creature_template SET lootid=60051 WHERE entry IN (60043,60047,59915);
-- Add Conquest Point On Kill
DELETE FROM creature_template_currency WHERE entry IN (60043,60047,60051,59915);
INSERT INTO creature_template_currency VALUES
(60043,390,40),
(60047,390,40),
(60051,390,40),
(59915,390,40);
INSERT INTO creature_loot_template VALUES
-- Group 1
(60051,85979,0,1,1,1,1),
(60051,89768,0,1,1,1,1),
(60051,85925,0,1,1,1,1),
(60051,89767,0,1,1,1,1),
(60051,85923,0,1,1,1,1),
(60051,85926,0,1,1,1,1),
(60051,85924,0,1,1,1,1),
(60051,85922,0,1,1,1,1),
(60051,85975,0,1,1,1,1),
(60051,85978,0,1,1,1,1),
(60051,85976,0,1,1,1,1),
(60051,85977,0,1,1,1,1),
(60051,89766,0,1,1,1,1),
(60051,86134,0,1,1,1,1),
-- Group 2
(60051,86380,0,1,2,1,1),
(60051,86283,0,1,2,1,1),
(60051,87409,0,1,2,1,1),
(60051,86238,0,1,2,1,1),
(60051,86379,0,1,2,1,1),
(60051,86382,0,1,2,1,1),
(60051,87410,0,1,2,1,1),
(60051,87408,0,1,2,1,1),
(60051,86272,0,1,2,1,1),
(60051,86279,0,1,2,1,1),
(60051,86280,0,1,2,1,1),
(60051,86281,0,1,2,1,1),
(60051,86284,0,1,2,1,1),
(60051,86297,0,1,2,1,1),
(60051,87411,0,1,2,1,1),
(60051,87413,0,1,2,1,1),
(60051,87412,0,1,2,1,1);

-- Correct loot Spirit King - Mogushan Vault 10M Normal
UPDATE creature_loot_template SET ChanceOrQuestChance=0 WHERE entry=60701;
UPDATE creature_loot_template SET groupid=1 WHERE item IN (86083,86084,86086,86127,86128,86129) AND entry=60701;
UPDATE creature_loot_template SET groupid=2 WHERE item IN (87051,87052,87053,87054,87055,89819,89936) AND entry=60701;
UPDATE creature_template SET lootid=0,mingold=0,maxgold=0 WHERE entry=60708;
DELETE FROM creature_loot_template WHERE entry=60708;
DELETE FROM creature_loot_template WHERE entry=60709;
UPDATE creature_template SET lootid=0 WHERE entry=60709;

-- Insert missing trinity_string
DELETE FROM trinity_string WHERE entry IN (5038,5037);
insert into `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`, `content_loc9`, `content_loc10`) values('5037','Mechanic Immune: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert into `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`, `content_loc9`, `content_loc10`) values('5038','Unit field flags: %u',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);


-- Open Dungeon --
-- Shado-Pan Monastery
DELETE FROM disables WHERE sourceType=2 AND entry=959;
-- Siege of Niuzao Temple
DELETE FROM disables WHERE sourceType=2 AND entry=1011;
-- Temple of the Jade Serpent
DELETE FROM disables WHERE sourceType=2 AND entry=960;
-- Stormstout Brewery
DELETE FROM disables WHERE sourceType=2 AND entry=961;

-- Correct for Player Start Zone --
DROP TABLE IF EXISTS `playercreateinfo`;
CREATE TABLE `playercreateinfo` (
  `race` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `class` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `map` smallint(5) unsigned NOT NULL DEFAULT '0',
  `zone` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position_x` float NOT NULL DEFAULT '0',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `orientation` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`race`,`class`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `playercreateinfo` */
-- LOCK TABLES `playercreateinfo` WRITE;
insert  into `playercreateinfo`(`race`,`class`,`map`,`zone`,`position_x`,`position_y`,`position_z`,`orientation`) values (1,9,0,9,-8914.57,-133.909,80.5378,5.13806),(1,10,0,9,-8914.57,-133.909,80.5378,5.13806),(1,8,0,9,-8914.57,-133.909,80.5378,5.13806),(1,5,0,9,-8914.57,-133.909,80.5378,5.13806),(1,4,0,9,-8914.57,-133.909,80.5378,5.13806),(1,2,0,9,-8914.57,-133.909,80.5378,5.13806),(1,1,0,9,-8914.57,-133.909,80.5378,5.13806),(2,1,1,14,-618.518,-4251.67,38.718,4.72222),(2,10,1,14,-618.518,-4251.67,38.718,4.72222),(2,3,1,14,-618.518,-4251.67,38.718,4.72222),(2,4,1,14,-618.518,-4251.67,38.718,4.72222),(2,7,1,14,-618.518,-4251.67,38.718,4.72222),(2,9,1,14,-618.518,-4251.67,38.718,4.72222),(3,1,0,1,-6240.32,331.033,382.758,6.17716),(3,10,0,1,-6240.32,331.033,382.758,6.17716),(3,2,0,1,-6240.32,331.033,382.758,6.17716),(3,3,0,1,-6240.32,331.033,382.758,6.17716),(3,4,0,1,-6240.32,331.033,382.758,6.17716),(3,5,0,1,-6240.32,331.033,382.758,6.17716),(4,1,1,141,10311.3,832.463,1326.41,5.69632),(4,10,1,141,10311.3,832.463,1326.41,5.69632),(4,3,1,141,10311.3,832.463,1326.41,5.69632),(4,4,1,141,10311.3,832.463,1326.41,5.69632),(4,5,1,141,10311.3,832.463,1326.41,5.69632),(4,11,1,141,10311.3,832.463,1326.41,5.69632),(5,9,0,5692,1699.85,1706.56,135.928,4.88839),(5,10,0,5692,1699.85,1706.56,135.928,4.88839),(5,8,0,5692,1699.85,1706.56,135.928,4.88839),(5,5,0,5692,1699.85,1706.56,135.928,4.88839),(5,3,0,5692,1699.85,1706.56,135.928,4.88839),(5,1,0,5692,1699.85,1706.56,135.928,4.88839),(6,7,1,221,-2915.55,-257.347,59.2693,0.302378),(6,10,1,221,-2915.55,-257.347,59.2693,0.302378),(6,5,1,221,-2915.55,-257.347,59.2693,0.302378),(6,2,1,221,-2915.55,-257.347,59.2693,0.302378),(6,1,1,221,-2915.55,-257.347,59.2693,0.302378),(7,9,0,5495,-4983.42,877.7,274.31,3.06393),(7,10,0,5495,-4983.42,877.7,274.31,3.06393),(7,1,0,5495,-4983.42,877.7,274.31,3.06393),(7,4,0,5495,-4983.42,877.7,274.31,3.06393),(7,8,0,5495,-4983.42,877.7,274.31,3.06393),(8,9,1,5691,-1171.45,-5263.65,0.847728,5.78945),(8,10,1,5691,-1171.45,-5263.65,0.847728,5.78945),(8,8,1,5691,-1171.45,-5263.65,0.847728,5.78945),(8,7,1,5691,-1171.45,-5263.65,0.847728,5.78945),(8,5,1,5691,-1171.45,-5263.65,0.847728,5.78945),(8,3,1,5691,-1171.45,-5263.65,0.847728,5.78945),(8,1,1,5691,-1171.45,-5263.65,0.847728,5.78945),(10,2,530,3431,10349.6,-6357.29,33.4026,5.31605),(10,10,530,3431,10349.6,-6357.29,33.4026,5.31605),(10,3,530,3431,10349.6,-6357.29,33.4026,5.31605),(10,4,530,3431,10349.6,-6357.29,33.4026,5.31605),(10,5,530,3431,10349.6,-6357.29,33.4026,5.31605),(10,8,530,3431,10349.6,-6357.29,33.4026,5.31605),(10,9,530,3431,10349.6,-6357.29,33.4026,5.31605),(11,1,530,3526,-3961.64,-13931.2,100.615,2.08364),(11,2,530,3526,-3961.64,-13931.2,100.615,2.08364),(11,3,530,3526,-3961.64,-13931.2,100.615,2.08364),(11,5,530,3526,-3961.64,-13931.2,100.615,2.08364),(11,7,530,3526,-3961.64,-13931.2,100.615,2.08364),(11,8,530,3526,-3961.64,-13931.2,100.615,2.08364),(1,3,0,9,-8914.57,-133.909,80.5378,5.13806),(11,6,609,4298,2358.17,-5663.21,426.027,3.93485),(11,10,530,3526,-3961.64,-13931.2,100.615,2.08364),(10,6,609,4298,2355.84,-5664.77,426.028,3.93485),(9,6,609,4298,2355.05,-5661.7,426.026,3.93485),(5,4,0,5692,1699.85,1706.56,135.928,4.88839),(6,3,1,221,-2915.55,-257.347,59.2693,0.302378),(7,5,0,5495,-4983.42,877.7,274.31,3.06393),(8,4,1,5691,-1171.45,-5263.65,0.847728,5.78945),(2,6,609,4298,2358.44,-5666.9,426.023,3.93485),(3,6,609,4298,2358.44,-5666.9,426.023,3.93485),(22,11,638,4714,-1443.62,1409.88,35.5561,3.19265),(22,9,638,4714,-1443.62,1409.88,35.5561,3.19265),(22,8,638,4714,-1443.62,1409.88,35.5561,3.19265),(22,6,609,4298,2355.84,-5662.21,426.028,3.93485),(22,4,638,4714,-1443.62,1409.88,35.5561,3.19265),(22,3,638,4714,-1443.62,1409.88,35.5561,3.19265),(22,1,638,4714,-1443.62,1409.88,35.5561,3.19265),(9,1,648,4765,-8423.81,1361.3,104.671,1.55428),(9,3,648,4765,-8423.81,1361.3,104.671,1.55428),(9,4,648,4765,-8423.81,1361.3,104.671,1.55428),(9,5,648,4765,-8423.81,1361.3,104.671,1.55428),(9,7,648,4765,-8423.81,1361.3,104.671,1.55428),(9,8,648,4765,-8423.81,1361.3,104.671,1.55428),(9,9,648,4765,-8423.81,1361.3,104.671,1.55428),(8,11,1,5691,-1171.45,-5263.65,0.847728,5.78945),(6,11,1,221,-2915.55,-257.347,59.2693,0.302378),(22,5,638,4714,-1443.62,1409.88,35.5561,3.19265),(2,8,1,14,-618.518,-4251.67,38.718,4.72222),(3,7,0,1,-6240.32,331.033,382.758,6.17716),(3,8,0,1,-6240.32,331.033,382.758,6.17716),(3,9,0,1,-6240.32,331.033,382.758,6.17716),(4,8,1,141,10311.3,832.463,1326.41,5.69632),(8,6,609,4298,2355.05,-5661.7,426.026,3.93485),(7,6,609,4298,2355.05,-5661.7,426.026,3.93485),(6,6,609,4298,2358.17,-5663.21,426.027,3.93485),(5,6,609,4298,2356.21,-5662.21,426.026,3.93485),(4,6,609,4298,2356.21,-5662.21,426.026,3.93485),(10,1,530,3431,10349.6,-6357.29,33.4026,5.31605),(1,6,609,4298,2355.84,-5664.77,426.028,3.93485),(24,1,860,5834,1470.97,3466.06,181.64,2.78136),(24,3,860,5834,1470.97,3466.06,181.64,2.78136),(24,4,860,5834,1470.97,3466.06,181.64,2.78136),(24,5,860,5834,1470.97,3466.06,181.64,2.78136),(24,7,860,5834,1470.97,3466.06,181.64,2.78136),(24,8,860,5834,1470.97,3466.06,181.64,2.78136),(24,10,860,5834,1470.97,3466.06,181.64,2.78136),(25,3,860,5834,1470.97,3466.06,181.64,2.78136),(25,8,860,5834,1470.97,3466.06,181.64,2.78136),(25,7,860,5834,1470.97,3466.06,181.64,2.78136),(25,5,860,5834,1470.97,3466.06,181.64,2.78136),(25,4,860,5834,1470.97,3466.06,181.64,2.78136),(25,1,860,5834,1470.97,3466.06,181.64,2.78136),(25,10,860,5834,1470.97,3466.06,181.64,2.78136),(26,3,860,5834,1470.97,3466.06,181.64,2.78136),(26,8,860,5834,1470.97,3466.06,181.64,2.78136),(26,7,860,5834,1470.97,3466.06,181.64,2.78136),(26,5,860,5834,1470.97,3466.06,181.64,2.78136),(26,4,860,5834,1470.97,3466.06,181.64,2.78136),(26,1,860,5834,1470.97,3466.06,181.64,2.78136),(26,10,860,5834,1470.97,3466.06,181.64,2.78136);

-- Update Worgen Start at Human ZONE
UPDATE playercreateinfo SET map=0,zone=9,position_x=-8914.57,position_y=-133.909,position_z=80.5378,orientation=5.13806 WHERE race=22 AND class!=6;
-- Update Goblin Start at Orc/ Troll ZONE
UPDATE playercreateinfo SET map=1,zone=14,position_x=-618.518,position_y=-4251.67,position_z=38.718,orientation=4.72222 WHERE race=9 AND class!=6;
DELETE FROM playercreateinfo_action;

------ DROP Mote of Hamory -----------
DELETE FROM creature_loot_template WHERE entry=65621;
INSERT INTO creature_loot_template VALUES 
(65621,89112,19,1,1,1,1),
(65621,72988,46,1,1,1,6),
(65621,81413,11,1,1,1,1),
(65621,81405,11,1,1,1,1),
(65621,81406,11,1,1,1,1),
(65621,87501,2,1,1,1,1);

-- Delete wrong item sell
DELETE FROM creature WHERE id IN (27816,27815);
DELETE FROM npc_vendor WHERE entry IN (27816,27815);
