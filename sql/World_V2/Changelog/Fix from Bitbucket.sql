-- Fix from Bitbucket
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=348 AND `spell_effect`=-108686;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (348, -108686, 1, 'Immolate removes Immolate (Fire and Brimstone)');
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=108686 AND `spell_effect`=-348;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (108686, -348, 1, 'Immolate (Fire and Brimstone) removes Immolate');
UPDATE `spell_proc_event` SET `SpellFamilyMask0`=1000 WHERE (`entry`=117896);
REPLACE INTO `spell_script_names` VALUES (117828, 'spell_warlock_backdraft_aura');
REPLACE INTO `spell_script_names` VALUES (117896, 'spell_warlock_backdraft_proc');
REPLACE INTO `spell_script_names` VALUES (116858, 'spell_warl_chaos_bolt');
DELETE FROM `spell_script_names` WHERE `spell_id`=29722 AND `ScriptName`='spell_warl_incinerate';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (29722, 'spell_warl_incinerate');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry`=61632;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `ConditionTypeOrReference`, `ConditionValue1`, `ConditionValue2`, `Comment`) VALUES (13, 1, 61632, 31, 3, 28860, 'Berserk of Sartharion');
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `ConditionTypeOrReference`, `ConditionValue1`, `ConditionValue2`, `Comment`) VALUES (13, 2, 61632, 31, 3, 28860, 'Berserk of Sartharion');
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `ConditionTypeOrReference`, `ConditionValue1`, `ConditionValue2`, `Comment`) VALUES (13, 3, 61632, 31, 3, 28860, 'Berserk of Sartharion');
DELETE FROM `spell_script_names` WHERE `spell_id`=61254 AND `ScriptName`='spell_will_of_sartharion';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (61254, 'spell_will_of_sartharion');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=13 AND `SourceEntry`=30531;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `ConditionTypeOrReference`, `ConditionValue1`, `ConditionValue2`, `Comment`) VALUES (13, 1, 30531, 31, 3, 17256, 'Soul Transfer');
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `ConditionTypeOrReference`, `ConditionValue1`, `ConditionValue2`, `Comment`) VALUES (13, 2, 30531, 31, 3, 17256, 'Soul Transfer');
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `ConditionTypeOrReference`, `ConditionValue1`, `ConditionValue2`, `Comment`) VALUES (13, 3, 30531, 31, 3, 17256, 'Soul Transfer');

DELETE FROM `spell_linked_spell` WHERE `spell_trigger`=120451 AND `spell_effect`=-108503;
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `comment`) VALUES (120451, -108503, 'Flames of Xoroth removes Grimoire of Sacrifice');


DELETE FROM spell_script_names WHERE ScriptName = 'spell_mage_glyph_of_illusion';
INSERT INTO spell_script_names (spell_id, ScriptName) VALUES (63092, 'spell_mage_glyph_of_illusion');
DELETE FROM spell_script_names WHERE ScriptName = 'spell_mage_glyph_of_conjure_familiar';
INSERT INTO spell_script_names (spell_id, ScriptName) VALUES (126748, 'spell_mage_glyph_of_illusion');


DELETE FROM spell_script_names WHERE spell_id = 123761 AND Scriptname = 'spell_monk_mana_tea';
INSERT INTO spell_script_names (spell_id, ScriptName) VALUES (123761, 'spell_monk_mana_tea');

DELETE FROM `spell_script_names` WHERE `ScriptName` = 'spell_gen_mixology_bonus';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(53755,'spell_gen_mixology_bonus'),
(53758,'spell_gen_mixology_bonus'),
(53760,'spell_gen_mixology_bonus'),
(54212,'spell_gen_mixology_bonus'),
(62380,'spell_gen_mixology_bonus'),
(53752,'spell_gen_mixology_bonus'),
(28521,'spell_gen_mixology_bonus'),
(42735,'spell_gen_mixology_bonus'),
(28518,'spell_gen_mixology_bonus'),
(28519,'spell_gen_mixology_bonus'),
(28540,'spell_gen_mixology_bonus'),
(28520,'spell_gen_mixology_bonus'),
(17629,'spell_gen_mixology_bonus'),
(17627,'spell_gen_mixology_bonus'),
(17628,'spell_gen_mixology_bonus'),
(17626,'spell_gen_mixology_bonus'),
(28497,'spell_gen_mixology_bonus'),
(60340,'spell_gen_mixology_bonus'),
(60341,'spell_gen_mixology_bonus'),
(60343,'spell_gen_mixology_bonus'),
(60344,'spell_gen_mixology_bonus'),
(60345,'spell_gen_mixology_bonus'),
(60346,'spell_gen_mixology_bonus'),
(53751,'spell_gen_mixology_bonus'),
(53764,'spell_gen_mixology_bonus'),
(53748,'spell_gen_mixology_bonus'),
(60347,'spell_gen_mixology_bonus'),
(53763,'spell_gen_mixology_bonus'),
(53747,'spell_gen_mixology_bonus'),
(53749,'spell_gen_mixology_bonus'),
(33721,'spell_gen_mixology_bonus'),
(53746,'spell_gen_mixology_bonus'),
(28514,'spell_gen_mixology_bonus'),
(28509,'spell_gen_mixology_bonus'),
(28503,'spell_gen_mixology_bonus'),
(28502,'spell_gen_mixology_bonus'),
(38954,'spell_gen_mixology_bonus'),
(39628,'spell_gen_mixology_bonus'),
(54494,'spell_gen_mixology_bonus'),
(39627,'spell_gen_mixology_bonus'),
(28501,'spell_gen_mixology_bonus'),
(28493,'spell_gen_mixology_bonus'),
(39626,'spell_gen_mixology_bonus'),
(33726,'spell_gen_mixology_bonus'),
(28491,'spell_gen_mixology_bonus'),
(39625,'spell_gen_mixology_bonus'),
(28490,'spell_gen_mixology_bonus'),
(54452,'spell_gen_mixology_bonus'),
(33720,'spell_gen_mixology_bonus'),
(24361,'spell_gen_mixology_bonus'),
(17539,'spell_gen_mixology_bonus'),
(17538,'spell_gen_mixology_bonus'),
(17537,'spell_gen_mixology_bonus'),
(17535,'spell_gen_mixology_bonus'),
(11348,'spell_gen_mixology_bonus'),
(11406,'spell_gen_mixology_bonus'),
(26276,'spell_gen_mixology_bonus'),
(11474,'spell_gen_mixology_bonus'),
(24363,'spell_gen_mixology_bonus'),
(11405,'spell_gen_mixology_bonus'),
(11334,'spell_gen_mixology_bonus'),
(11390,'spell_gen_mixology_bonus'),
(11396,'spell_gen_mixology_bonus'),
(11349,'spell_gen_mixology_bonus'),
(21920,'spell_gen_mixology_bonus'),
(11328,'spell_gen_mixology_bonus'),
(3223,'spell_gen_mixology_bonus'),
(3593,'spell_gen_mixology_bonus'),
(3164,'spell_gen_mixology_bonus'),
(7844,'spell_gen_mixology_bonus'),
(3160,'spell_gen_mixology_bonus'),
(3220,'spell_gen_mixology_bonus'),
(3222,'spell_gen_mixology_bonus'),
(63729,'spell_gen_mixology_bonus'),
(3166,'spell_gen_mixology_bonus'),
(8212,'spell_gen_mixology_bonus'),
(2374,'spell_gen_mixology_bonus'),
(2378,'spell_gen_mixology_bonus'),
(3219,'spell_gen_mixology_bonus'),
(2367,'spell_gen_mixology_bonus'),
(105694,'spell_gen_mixology_bonus'),
(105696,'spell_gen_mixology_bonus'),
(673,'spell_gen_mixology_bonus');

-- Add Rest of Items & Proper COST to the vendor Tan Shin Tiao
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64605, 0, 87548, 0, 0, 3890, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64605, 0, 87549, 0, 0, 3890, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64605, 0, 89363, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64605, 0, 89795, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64605, 0, 93230, 0, 0, 0, 1);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `type`) VALUES (64605, 0, 104198, 0, 0, 5229, 1);

-- Fix up the item in Tan Shin Tiao to add REP REQUIREMENT
UPDATE item_template SET RequiredReputationFaction = 1345 where entry = 104198;
UPDATE item_template SET RequiredReputationRank = 7 where entry = 104198;

-- Add the Horn to the NPC, should have 98% dropchance sungraze behemoth
REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (58895, 89682, 98, 1, 0, 1, 1);

-- And Add Thaft of Fur to the npc shoul dalso have 98 drop chance
REPLACE INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES (66587, 89770, 98, 1, 0, 1, 1);

