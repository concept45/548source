-- Gobject free from 100500 -> 120000
SET @GO :=100500;

-- Add Gobject for quest The Crumbling Hall (30277)
DELETE FROM gameobject WHERE id IN (214477);
INSERT INTO `gameobject` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`, `isActive`, `protect_anti_doublet`) VALUES
(@GO,'214393','870','0','0','1','1','1770.19','1957.94','240.349','1.15948','0','0','0.547805','0.836606','300','0','1','0',NULL),
(@GO+1,'214394','870','0','0','1','1','1770.19','1957.94','241.989','1.19875','0','0','0.564126','0.825689','300','0','1','0',NULL);
-- Respawn GO for quest Decryption Made Easy(205145)
DELETE FROM gameobject WHERE id IN (205145);
insert into `gameobject` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`, `isActive`, `protect_anti_doublet`) values
(@GO+2,'205145','646','0','0','1','1','585.833','1063.08','78.1651','6.15193','0','0','0.0655821','-0.997847','300','0','1','0',NULL),
(@GO+3,'205145','646','0','0','1','1','415.833','859.083','6.19448','2.73903','0','0','0.979811','0.199926','300','0','1','0',NULL),
(@GO+4,'205145','646','0','0','1','1','269.847','971.729','60.1537','1.66774','0','0','0.740538','0.672014','300','0','1','0',NULL),
(@GO+5,'205145','646','0','0','1','1','377.084','965.797','61.0831','3.21262','0','0','0.99937','-0.0355038','300','0','1','0',NULL),
(@GO+6,'205145','646','0','0','1','1','422.663','1126.11','61.2631','5.20674','0','0','0.512612','-0.85862','300','0','1','0',NULL),
(@GO+7,'205145','646','0','0','1','1','439.635','1148.97','60.5528','5.19103','0','0','0.519339','-0.854568','300','0','1','0',NULL),
(@GO+8,'205145','646','0','0','1','1','492.634','968.329','75.222','1.50437','0','0','0.683237','0.730196','300','0','1','0',NULL),
(@GO+9,'205145','646','0','0','1','1','546.812','999.376','79.5972','1.20906','0','0','0.568377','0.822768','300','0','1','0',NULL),
(@GO+10,'205145','646','0','0','1','1','587.166','1055.3','78.6301','3.27388','0','0','0.997813','-0.0660931','300','0','1','0',NULL);
-- Respawn GO for quest Seek Out the Lorewalker(29888)
DELETE FROM gameobject WHERE id IN (209845);
insert into `gameobject` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`, `isActive`, `protect_anti_doublet`) values
(@GO+11,'209845','870','0','0','1','1','-601.398','-2289.2','23.765','3.83491','0','0','0.940513','-0.339757','300','0','1','0',NULL);


-- Use Creature GUID from 8732 -> 9400
SET @ENTRY := 8732;

-- Add missing NPC for quest A Blight Upon the Land (28237)
DELETE FROM creature WHERE id=48080;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY,'48080','0','0','0','1','1','0','0','-512.449','32.8214','49.2409','0.814205','300','0','0','3372','0','0','0','0','0','0','0','0',NULL);
-- Add missing NPC Big Keech <Rare Antiquities>
DELETE FROM creature WHERE id=61650;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+1,'61650','870','0','0','1','1','0','0','1204.71','1042.33','417.975','4.91796','300','0','0','433335','0','0','0','0','0','0','0','0',NULL);
-- Add missing NPC: Arcanist Miluria <Legacy Justice Quartermaster> (35494)
DELETE FROM creature WHERE id=35494;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+2,'35494','571','0','0','1','1','0','35494','5767.09','737.875','653.666','2.989','300','0','0','10080','8814','0','0','0','0','0','0','0',NULL);
-- Add missing NPC: Kezzik the Striker <Gladiator, Merciless, & Vengeful Gear> (54650)
DELETE FROM creature WHERE id=54650;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+3,'54650','530','0','0','1','1','0','0','3075.67','3641.87','143.781','3.80412','300','0','0','8100','0','0','0','0','0','0','0','0',NULL);
-- Add missing NPC: Arcanist Uovril <Legacy Justice Quartermaster> (37942)
delete FROM creature WHERE id=37942;
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+4,'37942','571','0','0','1','1','0','37942','5757.76','745.831','653.664','4.69833','300','0','0','10080','8814','0','0','0','0','0','0','0',NULL);
-- Add missing NPC: Arcanist Adurin <Legacy Justice Quartermaster> (31579)
DELETE FROM creature WHERE id=31579;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+5,'31579','571','0','0','1','1','0','31579','5764.94','733.968','653.664','2.26752','300','0','0','10080','8814','0','0','0','0','0','0','0',NULL);
-- Add missing NPC: Arcanist Ivrenne <Legacy Justice Quartermaster> (31580)
DELETE FROM creature WHERE id=31580;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+6,'31580','571','0','0','1','1','0','31580','5764.13','745.676','653.665','4.19173','300','0','0','10080','8814','0','0','0','0','0','0','0',NULL);
-- Add missing NPC: Toren Landow <Legacy Justice Quartermaster> (58154)
DELETE FROM creature WHERE id=58154;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+7,'58154','0','0','0','1','1','0','0','-8808','349.775','107.049','3.84225','300','0','0','3660','5013','0','0','0','0','0','0','0',NULL);
-- Add missing NPC: Magatha Silverton <Justice Quartermaster> (44246)
DELETE FROM creature WHERE id=44246;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+8,'44246','0','0','0','1','1','0','0','-8803.37','350.744','108.864','3.89252','300','0','0','3660','5013','0','0','0','0','0','0','0',NULL);
-- Add missing NPC: Faldren Tillsdale <Valor Quartermaster> (44245)
DELETE FROM creature WHERE id=44245;
DELETE FROM creature WHERE id IN (40216,20278,69323,69979);
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+9,'44245','0','0','0','1','1','0','0','-8801.33','348.296','108.839','3.92316','300','0','0','3660','5013','0','0','0','0','0','0','0',NULL),
(@ENTRY+10,'20278','1','0','0','1','1','0','0','-7120.84','-3774.95','8.89802','0.922678','300','0','0','6986','0','0','0','0','0','0','0','0',NULL),
(@ENTRY+11,'40216','1','0','0','1','1','0','40216','-7118.8','-3777.37','8.72798','0.572391','300','0','0','8100','0','0','0','0','0','0','0','0',NULL),
(@ENTRY+12,'69323','1','0','0','1','1','0','0','-7123.69','-3772.31','9.10835','0.91718','300','0','0','8982','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+13,'69979','1','0','0','1','1','0','0','-7126.11','-3769.34','9.20894','0.940108','300','0','0','8982','0','0','0','0','0','2048','0','0',NULL);
-- Creature for quest Backed Into a Corner(30286)
DELETE FROM creature WHERE id IN (63949,64186);
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+14,'63949','870','0','0','1','1','0','0','715.044','1511.25','385.725','2.17579','300','0','0','84','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+15,'63949','870','0','0','1','1','0','0','742.021','1491.65','386.084','1.49951','300','0','0','84','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+16,'63949','870','0','0','1','1','0','0','756.079','1503.3','384.066','2.46634','300','0','0','84','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+17,'63949','870','0','0','1','1','0','0','728.838','1506.75','385.321','6.24724','300','0','0','84','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+18,'63949','870','0','0','1','1','0','0','729.373','1524.47','385.667','5.14061','300','0','0','84','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+19,'63949','870','0','0','1','1','0','0','793.009','1529.95','386.153','3.65857','300','0','0','84','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+20,'64186','870','0','0','1','1','0','0','794.816','1526.95','386.52','3.65779','300','0','0','393941','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+21,'64186','870','0','0','1','1','0','0','758.792','1502.54','383.566','3.65386','300','0','0','393941','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+22,'64186','870','0','0','1','1','0','0','742.659','1494.08','386.03','2.98627','300','0','0','393941','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+23,'64186','870','0','0','1','1','0','0','731.282','1506.24','385.363','6.26138','300','0','0','393941','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+24,'64186','870','0','0','1','1','0','0','714.042','1512.12','385.761','1.92363','300','0','0','393941','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+25,'64186','870','0','0','1','1','0','0','726.196','1523.88','385.582','2.80407','300','0','0','393941','0','0','0','0','0','2048','0','0',NULL);
-- Respawn creature for quest The Secrets of Guo-Lai (30639)
DELETE FROM creature WHERE id IN (64647,64663);
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+26,'64647','870','0','0','1','1','0','0','1738.05','1915.09','223.266','5.75755','300','0','0','1969705','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+27,'64663','870','0','0','1','1','0','0','1730.57','1895.05','223.266','6.2013','300','0','0','2940','0','0','0','0','0','2048','0','0',NULL);
-- Respawn for quest The Point of No Return
DELETE FROM creature WHERE id=61055;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+28,'61055','870','0','0','1','1','0','0','1823.57','2201.38','391.886','2.67016','300','0','0','759850','0','0','0','0','0','2048','0','0',NULL);
-- Respawn for quest The Challenger's Ring: Chao the Voice
DELETE FROM creature WHERE id=63905;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) VALUES
(@ENTRY+29,'63905','870','0','0','1','1','0','0','1961.46','4176.91','138.468','2.58511','300','0','0','393941','0','0','0','0','0','2048','0','0',NULL);
-- Respawn for quest The Challenger's Ring: Hawkmaster Nurong (31220)
DELETE FROM creature WHERE id=62600;
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+30,'62600','870','0','0','1','1','0','0','1956.26','4171.6','138.469','2.16964','300','0','0','393941','0','0','0','0','0','2048','0','0',NULL);
-- Respawn for quest The Challenger's Ring: Lao-Chin the Iron Belly (31128)
DELETE FROM creature WHERE id=62093;
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+31,'62093','870','0','0','1','65535','0','0','1862.07','4235.91','148.764','3.16458','300','0','0','393941','0','0','0','0','0','2048','0','0',NULL);
-- Respawn for quest The Challenger's Ring: Snow Blossom (31038)
DELETE FROM creature WHERE id=62782;
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+32,'62782','870','0','0','1','1','0','0','1950.67','4169.26','138.469','1.74136','300','0','0','787882','0','0','0','0','0','2048','0','0',NULL);
-- Respawn for quest The Challenger's Ring: Yalia Sagewhisper (31104)
DELETE FROM creature WHERE id=62828;
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+33,'62828','870','0','0','1','1','0','0','1852.55','4247.67','148.745','3.94511','300','0','0','787882','0','0','0','0','0','2048','0','0',NULL);
-- Respawn for quest Friends, Not Food! (31201)
DELETE FROM creature WHERE id IN (64459,64460,64461);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+34,'64459','870','0','0','1','65535','0','0','2514.45','6059.57','54.301','3.67729','300','0','0','393941','0','0','1','0','0','2048','0','0',NULL),
(@ENTRY+35,'64460','870','0','0','1','65535','0','0','2339.24','6076.82','55.4635','3.52963','300','0','0','393941','0','0','1','0','0','2048','0','0',NULL),
(@ENTRY+36,'64461','870','0','0','1','65535','0','0','2403.86','6093.18','55.0588','4.21293','300','0','0','393941','0','0','1','0','0','2048','0','0',NULL);
-- Respawn for quest Spirits of the Water(29894)
DELETE FROM creature WHERE id=56398;
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+37,'56398','870','0','0','1','1','0','0','-289.16','-2630.65','1.1381','5.5824','300','0','0','84','0','0','0','0','0','2048','0','0',NULL);
-- Respawn for quest Helping the Cause(30568)
DELETE FROM creature WHERE id in (59609,59572);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+38,'59572','870','0','0','1','1','0','0','-238.675','-2668.46','0.88292','0.10187','300','0','0','156000','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+39,'59572','870','0','0','1','1','0','0','-234.051','-2618.31','0.0727101','4.81033','300','0','0','156000','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+40,'59572','870','0','0','1','1','0','0','-194.98','-2629.61','0.534697','4.20871','300','0','0','156000','0','0','0','0','0','2048','0','0',NULL),
(@ENTRY+41,'59609','870','0','0','1','1','0','0','-383.159','-2734.15','5.38747','3.67857','300','0','0','156000','0','0','0','0','0','32','0','0',NULL),
(@ENTRY+42,'59609','870','0','0','1','1','0','0','-403.402','-2769.17','10.0346','3.43118','300','0','0','156000','0','0','0','0','0','32','0','0',NULL),
(@ENTRY+43,'59609','870','0','0','1','1','0','0','-445.453','-2768.02','9.12743','5.73004','300','0','0','156000','0','0','0','0','0','32','0','0',NULL),
(@ENTRY+44,'59609','870','0','0','1','1','0','0','-450.709','-2713.56','5.20029','2.0316','300','0','0','156000','0','0','0','0','0','32','0','0',NULL),
(@ENTRY+45,'59609','870','0','0','1','1','0','0','-405.902','-2679.91','6.56949','4.84333','300','0','0','156000','0','0','0','0','0','32','0','0',NULL);
-- Respawn for quest Family Heirlooms(29762)
DELETE FROM creature WHERE id in (59348);
insert into `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `npcflag2`, `unit_flags`, `unit_flags2`, `dynamicflags`, `isActive`, `protec_anti_doublet`) values
(@ENTRY+46,'59348','870','0','0','1','1','0','0','-409.021','-3128.28','31.317','2.02849','300','0','0','156000','0','0','0','0','0','0','0','0',NULL),
(@ENTRY+47,'59348','870','0','0','1','1','0','0','-438.875','-3138.29','31.3171','3.45242','300','0','0','156000','0','0','0','0','0','0','0','0',NULL),
(@ENTRY+48,'59348','870','0','0','1','1','0','0','-465.967','-3184.45','32.8445','1.31928','300','0','0','156000','0','0','0','0','0','0','0','0',NULL),
(@ENTRY+49,'59348','870','0','0','1','1','0','0','-390.529','-3216.6','33.337','3.32236','300','0','0','156000','0','0','0','0','0','0','0','0',NULL),
(@ENTRY+50,'59348','870','0','0','1','1','0','0','-374.025','-3247.33','34.4942','5.96208','300','0','0','156000','0','0','0','0','0','0','0','0',NULL),
(@ENTRY+51,'59348','870','0','0','1','1','0','0','-397.553','-3266.98','31.2828','5.72488','300','0','0','156000','0','0','0','0','0','0','0','0',NULL),
(@ENTRY+52,'59348','870','0','0','1','1','0','0','-435.957','-3297.16','33.402','4.90492','300','0','0','156000','0','0','0','0','0','0','0','0',NULL),
(@ENTRY+53,'59348','870','0','0','1','1','0','0','-446.143','-3212.8','40.5575','4.20278','300','0','0','156000','0','0','0','0','0','0','0','0',NULL);
--


